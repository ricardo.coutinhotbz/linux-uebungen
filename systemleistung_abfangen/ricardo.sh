#!/bin/bash

LOG_VERZEICHNIS="/mnt/c/bash_ha/b/"
HTML_DATEI="/mnt/c/bash_ha/b/index.html"
LOG_DATEI="${LOG_VERZEICHNIS}2024-06-sys-NB-I003.log"
#dies ist ein kommentar 
# Sicherstellen, dass das Verzeichnis existiert
mkdir -p $LOG_VERZEICHNIS

# Logdatei schreiben
echo "Skript gestartet: $(date)" >> $LOG_DATEI

# Funktion zum Abrufen von Systeminformationen und Ausgabe in die Konsole und HTML-Datei
systeminformationen_abrufen() {
  # Systeminformationen sammeln
  sys_betriebszeit=$(uptime -p)
  sys_aktuelle_zeit=$(date '+%Y-%m-%d %H:%M:%S')
  speicherplatz_frei=$(df -h / | awk 'NR==2 {print $4}')
  speicherplatz_belegt=$(df -h / | awk 'NR==2 {print $3}')
  sys_rechnername=$(hostname)
  sys_ip_adresse=$(hostname -I | awk '{print $1}')
  betriebssystem=$(uname -o)
  os_version=$(uname -r)
  prozessor_modell=$(lscpu | grep "Model name" | awk -F: '{print $2}' | sed 's/^ *//')
  prozessor_kerne=$(nproc)
  speicher_gesamt=$(free -h | awk 'NR==2 {print $2}')
  speicher_genutzt=$(free -h | awk 'NR==2 {print $3}')
  speicher_frei=$(free -h | awk 'NR==2 {print $4}')
  
  # Berechnung des genutzten Speicherplatzes in Prozent
  speicherplatz_belegt_prozent=$(df / | awk 'NR==2 {print $5}' | sed 's/%//')
  speicher_genutzt_prozent=$(free | awk 'NR==2 {printf "%.0f", $3/$2 * 100}')

  # Systeminformationen anzeigen
  printf "+--------------------------------+---------------------------+\n"
  printf "| %-30s | %-25s |\n" "Text" "Wert"
  printf "+--------------------------------+---------------------------+\n"
  printf "| %-30s | %-25s |\n" "Rechnername" "$sys_rechnername"
  printf "| %-30s | %-25s |\n" "IP Adresse" "$sys_ip_adresse"
  printf "| %-30s | %-25s |\n" "Betriebszeit" "$sys_betriebszeit"
  printf "| %-30s | %-25s |\n" "Aktuelle Zeit" "$sys_aktuelle_zeit"
  printf "| %-30s | %-25s |\n" "Betriebssystem" "$betriebssystem"
  printf "| %-30s | %-25s |\n" "OS Version" "$os_version"
  printf "| %-30s | %-25s |\n" "Prozessormodell" "$prozessor_modell"
  printf "| %-30s | %-25s |\n" "Anzahl der Kerne" "$prozessor_kerne"
  printf "| %-30s | %-25s |\n" "Gesamter Speicher" "$speicher_gesamt"
  printf "| %-30s | %-25s |\n" "Genutzter Speicher" "$speicher_genutzt"
  printf "| %-30s | %-25s |\n" "Freier Speicher" "$speicher_frei"
  printf "| %-30s | %-25s |\n" "Freier Speicherplatz" "$speicherplatz_frei"
  printf "| %-30s | %-25s |\n" "Belegter Speicherplatz" "$speicherplatz_belegt"
  printf "+--------------------------------+---------------------------+\n"

  # Informationen in eine temporäre Datei schreiben
  {
    echo "---------------------"
    echo "Rechnername|$sys_rechnername"
    echo "IP Adresse|$sys_ip_adresse"
    echo "Betriebszeit|$sys_betriebszeit"
    echo "Aktuelle Zeit|$sys_aktuelle_zeit"
    echo "Betriebssystem|$betriebssystem"
    echo "OS Version|$os_version"
    echo "Prozessormodell|$prozessor_modell"
    echo "Anzahl der Kerne|$prozessor_kerne"
    echo "Gesamter Speicher|$speicher_gesamt"
    echo "Genutzter Speicher|$speicher_genutzt"
    echo "Freier Speicher|$speicher_frei"
    echo "Freier Speicherplatz|$speicherplatz_frei"
    echo "Belegter Speicherplatz|$speicherplatz_belegt"
    echo "Belegter Speicherplatz Prozent|$speicherplatz_belegt_prozent"
    echo "Genutzter Speicher Prozent|$speicher_genutzt_prozent"
  } > /tmp/system_info.tmp
}

# Funktion zum Protokollieren von Systeminformationen in eine Datei
systeminformationen_protokollieren() {
  cat /tmp/system_info.tmp >> $LOG_DATEI
}

# Funktion zum Erstellen einer HTML-Datei mit Systeminformationen
html_erstellen() {
  html_inhalt="<html><head><title>System Monitoring</title><style>
  table {width: 100%; border-collapse: collapse;}
  th, td {border: 1px solid black; padding: 8px; text-align: left;}
  th {background-color: #f2f2f2;}
  .green {background-color: #d4edda;}
  .red {background-color: #f8d7da;}
  </style></head><body><h1>System Monitoring</h1><table><tr><th>Text</th><th>Wert</th><th>Status</th></tr>"

  while IFS="|" read -r text wert; do
    if [[ "$text" == "Belegter Speicherplatz Prozent" || "$text" == "Genutzter Speicher Prozent" || "$text" == "Genutzter Speicher" || "$text" == "Belegter Speicherplatz" ]]; then
      if [[ "$wert" == *% ]]; then
        wert_num=${wert%%%}  # Entfernt das Prozentzeichen
      else
        wert_num=${wert%%[A-Za-z]*}  # Entfernt Einheiten wie Mi, Gi
      fi
      
      if [ "$wert_num" -lt 50 ]; then
        status_class="green"
        status_text="OK"
      else
        status_class="red"
        status_text="High"
      fi
      html_inhalt+="<tr><td>$text</td><td>$wert</td><td class=\"$status_class\">$status_text</td></tr>"
    else
      html_inhalt+="<tr><td>$text</td><td>$wert</td><td></td></tr>"
    fi
  done < /tmp/system_info.tmp

  html_inhalt+="</table></body></html>"
  echo "$html_inhalt" > $HTML_DATEI
}

# Hauptskript
if [ "$1" == "-f" ]; then
  echo "Option -f angegeben" >> $LOG_DATEI
  systeminformationen_abrufen
  systeminformationen_protokollieren
  html_erstellen
else
  echo "Keine Option angegeben" >> $LOG_DATEI
  systeminformationen_abrufen
  html_erstellen
fi

echo "Skript beendet: $(date)" >> $LOG_DATEI
rm -f /tmp/system_info.tmp
